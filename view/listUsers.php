<?php
include('../autoloaded.php');

$userController = new UserController();
$users = $userController->listAction();

if (count($users) == 0) {
    $view = $userController->newAction();

    header('location:'.$view);
    die;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Listar Usuarios</title>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
    </tr>
    </thead>

    <?php
    foreach ($users as $key => $user) {
        ?>
        <tr>
            <td><?php echo $key; ?></td>
            <td><?php echo $user; ?></td>
        </tr>
        <?php
    }
    ?>


</table>
</body>
</html>
