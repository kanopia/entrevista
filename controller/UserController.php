<?php


class UserController
{

    public function newAction()
    {
        return 'view/newUser.html';
    }

    public function listAction()
    {
        $connexion = new Connexion();

        $result = $connexion->generateQuerySelect('users');

        $users = [];
        if ($result->num_rows > 0) {
            while ($userTemp = $result->fetch_assoc()) {
                $users[$userTemp['id']] = $userTemp['name'];
            }
        }

        return $users;
    }

    public function newUser($user)
    {
        $connexion = new Connexion();

        return $connexion->generateQueryInsert('users', $user);
    }

}
