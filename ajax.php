<?php

include("autoloaded.php");

$userController = new UserController();

if (isset($_POST)) {

    if ($_POST['ac'] == 'new') {

        $data = $_POST;
        unset($data['ac']);

        $result = $userController->newUser($data);

        if ($result) {
            echo 'Usuario Registrado';

            return true;
        }

        return false;
    }

}
if (isset($_GET)) {

    if ($_GET['ac'] == 'list') {

        header('location: view/listUsers.php');
        die;

    }
}

