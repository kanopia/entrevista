<?php

class Connexion
{
    /**
     * @var mysqli
     */
    protected static $DBConnection = null;

    private $host = '127.0.0.1';
    private $user = 'root';
    private $pass = 'krsjz3dz';
    private $db = 'lucho';

    public function connectDB()
    {
        if (self::$DBConnection) {
            return self::$DBConnection;
        }

        self::$DBConnection = mysqli_connect($this->host, $this->user, $this->pass, $this->db);

        if (self::$DBConnection->connect_error) {
            return false;
        }

        return self::$DBConnection;
    }

    public function generateQueryInsert($table, $params)
    {
        $this->connectDB();

        $query = 'INSERT INTO '.$table.' VALUES(null,';

        foreach ($params as $value) {
            $query .= "'{$value}'";
        }

        $query .= ')';

        return self::$DBConnection->query($query);
    }

    public function generateQuerySelect($table, $conditions = null)
    {
        $this->connectDB();

        $query = 'SELECT * FROM '.$table.' WHERE 1';

        if (is_array($conditions) && count($conditions)) {
            foreach ($conditions as $key => $value) {
                $query .= 'AND '.$key.' = '.$value;
            }
        }

        return self::$DBConnection->query($query);

    }

}
